MKDIR := mkdir -p

CFLAGS := -Wall -Wextra -Wpedantic

BUILD_TYPE := release
TARGET_MACHINE := $(shell $(CC) -dumpmachine)
SRC := src
INC := inc

BUILD := build/$(TARGET_MACHINE)/$(BUILD_TYPE)
OUT := out/$(TARGET_MACHINE)/$(BUILD_TYPE)
CREATEDIRS := $(BUILD) $(OUT)
IPADDR_SRC := $(SRC)/ipaddr.c
IPADDR_OBJ := $(patsubst $(SRC)/%.c,$(BUILD)/%.o,$(IPADDR_SRC))

ifeq ($(BUILD_TYPE), debug)
	CFLAGS += -g -O0
else
	CFLAGS += -DNDEBUG
endif


.PHONY: help sharedlib staticlib tests clean

help:
	$(info USAGE: make <sharedlib|staticlib|tests|clean|help>)
	$(info ... [BUILD_TYPE=<debug|release>])

clean:
	$(RM) -r build out

sharedlib: CFLAGS += -fpic
sharedlib: $(OUT)/ipaddr.so

staticlib: $(OUT)/ipaddr.a

$(BUILD)/%.o: $(SRC)/%.c | $(CREATEDIRS)
	$(CC) -c $(CFLAGS) -I$(INC) -o $@ $^

$(OUT)/ipaddr.so: $(IPADDR_OBJ) | $(CREATEDIRS)
	$(CC) -shared -o $@ $^

#TODO: static lib

$(CREATEDIRS):
	$(MKDIR) $@
