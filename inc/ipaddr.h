#ifndef _IP_ADDR_H
#define _IP_ADDR_H

#define IPV4_LENGTH 4
#define IPV6_LENGTH 16

enum ipaddr_type {
  IPv4 = 4,
  IPv6 = 6,
};

struct ipaddr;

#endif
