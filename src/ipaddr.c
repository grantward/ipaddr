#include "ipaddr.h"

#include <inttypes.h>

struct ipaddr {
  enum ipaddr_type type;
  union {
    union {
      //IPv4
      uint8_t ipv4_bytes[IPV4_LENGTH];
      uint32_t ipv4_number;
    };
    union {
      //IPv6
      uint8_t ipv6_bytes[IPV6_LENGTH];
    };
  };
};
